export class Defaults {
  /**
   * Funktion gibt den Standardaufbau für
   */
  static runA11y() {
    it('Has no detectable a11y violations on load', () => {
      // Test the page at initial load
      cy.checkA11y({},
        {
          // @ts-ignore
          rules: {
            'color-contrast': {enabled: false},
          }
        }
      )
    });
  }

}
