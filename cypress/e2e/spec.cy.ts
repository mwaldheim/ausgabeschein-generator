import {Defaults} from "./defaults.cy";
import {LANGUAGES} from "../../src/app/shared/components/language/language";
// Description anpassen
//<editor-fold desc="Grundaufbau">
describe('Test',() => {
  // Durchlaufen der Tests für iPhones
  context('Run on iPhone-Viewport', () => {
    beforeEach(() => {
      cy.viewport('iphone-x');
      setURL();
    });
    runTest();
  })
  // Durchlaufen der Tests für iPads
  context('Run on iPad-Viewport', () => {
    beforeEach(() => {
      cy.viewport('ipad-2');
      setURL();
    });
    runTest();
  })
  // Durchlaufen der Tests für Desktops
  context('Run on Desktop-Viewport', () => {
    beforeEach(() => {
      cy.viewport('macbook-15');
      setURL();
    });
    runTest();
  })
})
//</editor-fold>

function setURL() {
  cy.visit('/');
  cy.injectAxe();
}

function runTest() {
  Defaults.runA11y();
  // Hier die Tests einfügen
  //<editor-fold desc="languages">
  it('should change Language and check if all Languages availabel', function () {
    cy.contains('Deutsch');

    for (let lang of LANGUAGES) {
      cy.get('.cy-language').click();
      cy.get('.cy-'+lang.iso).click();
      cy.contains(lang.name);
    }

    cy.get('.cy-language').click();
    cy.get('.cy-de').click();
  });
  //</editor-fold>W
  //<editor-fold desc="device">
  it('should search for device', function () {
    cy.intercept('GET', 'http://localhost:3004/normal', { fixture: 'normal.json' }).as('getDevice');
    let search = cy.get('input[type="search"]');
    let toggle = cy.get('button[role="switch"]');


    toggle.click();
    toggle.click();

    search.type('test');
    cy.wait('@getDevice');
    let results = cy.get('mat-option').first();
    results.click();


    let output = cy.get('button.cy-button-out');
    output.click();
    cy.contains('Vorschau - Ausgabeprotokoll');
    cy.get('.cy-print-close').click();

    let input = cy.get('button.cy-button-in');
    input.click();
    cy.contains('Vorschau - Rückgabeprotokoll');
    cy.get('.cy-print-close').click();

    let transfer = cy.get('button.cy-button-transfer');
    transfer.click();
    cy.contains('Vorschau - Überlassungsprotokoll');
    cy.get('.cy-print-close').click();



  });
  //</editor-fold>
  //<editor-fold desc="changelog">
  it('should open changelog', function () {
    cy.get('.cy-changelog').click();
    cy.get('.cy-changelog-close').click();
  });
  //</editor-fold>
  //<editor-fold desc="settings">
  it('should open settings', function () {
    cy.get('.cy-button-settings').click();
    cy.get('.cy-setting-close').click();
  });
  //</editor-fold>
}
