## [2.14.2](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.14.1...v2.14.2) (2025-01-22)


### Bug Fixes

* update to angular 19.1 ([756a447](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/756a447c266a3c7f1bf91078a95a98b46b4e3e59))

## [2.14.1](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.14.0...v2.14.1) (2024-11-26)


### Bug Fixes

* change gitlab assets for release ([1b2daa3](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/1b2daa37a9811cfd929e87aeab3c87ac18cf7129))

# [2.14.0](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.13.0...v2.14.0) (2024-11-20)


### Bug Fixes

* add sr for better control ([84388c3](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/84388c39fc92f841a86880023c9e57609123f237))


### Features

* Upgrade to latest angular v19 (Nightly Release) ([ef6e123](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/ef6e1235b8d43535cc44aa74b26999bc5d95f30e))

## [2.13.0](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.12.3...v2.13.0) (2024-06-05)

### :sparkles: Features

* Upgrade to Angular 18 ([cc5310d](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/cc5310d3e2bf78676e0d762d488ed8ecce8eb471))

### :bug: Fixes

* Add fixes of Migration to M3 ([eeee982](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/eeee98240d8c40ae204a1b4afe9ac75321a8e7c1))
* remove experimental ([19bf3d4](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/19bf3d4c7eb01e278b133804f8e122f6f33c9372))

### :repeat: CI

* remove licence check ([fb0efe4](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/fb0efe42c3911afe99d1bbfa5fe891cd68937476))

### :repeat: Build System

* Change Angular 18 Migration ([6c8b479](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/6c8b479a8a668f79366f69b9d707aa4e1a6bb4dd))

## [2.12.3](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.12.2...v2.12.3) (2024-03-15)


### :bug: Fixes

* Update to Angular 17.3 ([f8d86b7](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/f8d86b7f1f51e5f4e6a859276944a466e6f97ce1))


### :fast_forward: Performance Improvements

* Migration to new performance optimizations with signal methods ([93c4eb3](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/93c4eb399f1be2d7fa5f42c4b2968dbf2c502b1e))

## [2.12.2](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.12.1...v2.12.2) (2024-02-15)


### :bug: Fixes

* Update to Angular 17.2 ([0f3c8cb](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/0f3c8cbd887bd288fadc8499686a490610b2737f))


### :barber: Style

* Optimize Settings-view ([e23b482](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/e23b482753faf47c3dbec9909d173b1669c65c2d))
* Upgrade to M3 experimental ([b227054](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/b22705497dd39179f7b81a08c6d307484291ed5d))

## [2.12.1](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.12.0...v2.12.1) (2024-02-05)


### :bug: Fixes

* Add Defer as loading for Changelog ([d3cb2c8](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/d3cb2c85949ad3d6193919e0aa8c4b1f1925187b))
* Rollback Version of marked-Lib for Loading of Markdown ([609a3a3](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/609a3a36b0f1e419add2fb2d899d40e38af1386e))

## [2.12.0](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.11.2...v2.12.0) (2024-02-01)


### :sparkles: Features

* Add Arabic as new language ([5092252](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/5092252efa12a1326b96200a0cffd9a89ccebde3))
* change dependencies to Angular 17 ([6824a48](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/6824a487c8a7b657c88160ab3e337370217599c1))
* Migration to new angular control flow ([711d7e4](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/711d7e42f15d5480b2f14ed1ff55630a3176c6dd))
* Upgrade to Angular 17.1 ([8dbadf9](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/8dbadf925cdc4f5c758bfea5262e680480040c9b))


### :bug: Fixes

* Change settings for better ChangeDetection ([c15c71c](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/c15c71c2e899f52735289ba8ec85f10359088ba7))


### :barber: Style

* change primary-color of navigation with Actions ([86c77c4](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/86c77c4608f57b1a8148c335e7b027b1c2f70835))
* fix positioning of translation-overviews ([c0f85d5](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/c0f85d5d412202910722d8722b0c525e3ccf0ac3))
* Update color-system to new brand-colors ([1f6bc9e](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/1f6bc9eea1a1b050774a2bf999f50d863f7c0a0b))


### :repeat: Chores

* **release:** 2.12.0-beta.1 [skip ci] ([a289579](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/a289579a547fad79b9e127f22e1d7cd83a4beecf))
* **release:** 2.12.0-rc.1 [skip ci] ([cb7cd30](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/cb7cd308ca3264e9e13f72cf15a90d2c85206b5c))
* **release:** 2.12.0-rc.2 [skip ci] ([b158f6c](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/b158f6cf32c32a0df35b5de5117f7b2ad6b6b37f))
* Update cypress and jest ([0f33486](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/0f334866e28b9c992b0a11bae9744376af453add))

## [2.12.0-rc.2](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.12.0-rc.1...v2.12.0-rc.2) (2024-02-01)


### :sparkles: Features

* Add Arabic as new language ([5092252](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/5092252efa12a1326b96200a0cffd9a89ccebde3))
* Upgrade to Angular 17.1 ([8dbadf9](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/8dbadf925cdc4f5c758bfea5262e680480040c9b))

## [2.12.0-rc.1](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.11.2...v2.12.0-rc.1) (2023-11-08)


### :sparkles: Features

* change dependencies to Angular 17 ([6824a48](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/6824a487c8a7b657c88160ab3e337370217599c1))
* Migration to new angular control flow ([711d7e4](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/711d7e42f15d5480b2f14ed1ff55630a3176c6dd))


### :bug: Fixes

* Change settings for better ChangeDetection ([c15c71c](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/c15c71c2e899f52735289ba8ec85f10359088ba7))


### :barber: Style

* change primary-color of navigation with Actions ([86c77c4](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/86c77c4608f57b1a8148c335e7b027b1c2f70835))
* fix positioning of translation-overviews ([c0f85d5](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/c0f85d5d412202910722d8722b0c525e3ccf0ac3))
* Update color-system to new brand-colors ([1f6bc9e](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/1f6bc9eea1a1b050774a2bf999f50d863f7c0a0b))


### :repeat: Chores

* **release:** 2.12.0-beta.1 [skip ci] ([a289579](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/a289579a547fad79b9e127f22e1d7cd83a4beecf))
* Update cypress and jest ([0f33486](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/0f334866e28b9c992b0a11bae9744376af453add))

## [2.12.0-beta.1](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.11.1...v2.12.0-beta.1) (2023-11-08)


### :sparkles: Features

* change dependencies to Angular 17 ([6824a48](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/6824a487c8a7b657c88160ab3e337370217599c1))
* Migration to new angular control flow ([711d7e4](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/711d7e42f15d5480b2f14ed1ff55630a3176c6dd))


### :bug: Fixes

* Einfügen von Toggle-Box für einen Expliziten Aufruf ([b8b86ff](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/b8b86ff84ec5834fa60789be7f8e59c80bf06a7e))


### :repeat: Chores

* Erweitern der E2E-Tests ([5fe52e4](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/5fe52e48acb0264c87299d117ba3bec5f99b84af))

## [2.11.2](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.11.1...v2.11.2) (2023-09-05)


### :bug: Fixes

* Einfügen von Toggle-Box für einen Expliziten Aufruf ([b8b86ff](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/b8b86ff84ec5834fa60789be7f8e59c80bf06a7e))


### :repeat: Chores

* Erweitern der E2E-Tests ([5fe52e4](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/5fe52e48acb0264c87299d117ba3bec5f99b84af))

## [2.11.1](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.11.0...v2.11.1) (2023-08-30)


### :bug: Fixes

* Preadded URL-Support ([33ee9f3](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/33ee9f3e0e113121c898ac1d17ec7aa6209bc112))
* Update to Angular 16.2 and other Dependencies ([0488a20](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/0488a203cebc59e0065436b1645a92d0f7a82005))

## [2.11.0](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.10.1...v2.11.0) (2023-07-11)


### :repeat: Build System

* Optmize Angular with esbuild ([3c3b709](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/3c3b7094e2d7798b2742f4834cdc2a5cbf9286c2))
* Update to newest Dependencies (Cypress, Jest, Node) ([61a17ed](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/61a17edc1edfb6b5757c6b10c6fb310735a17303))


### :bug: Fixes

* Add Fetch for HTTP ([3abb76d](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/3abb76d6b588e88867da45d831e057e667032e13))


### :sparkles: Features

* Add Autosearch by Referrer of this Page ([6a25e88](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/6a25e883acd3e8ed1899711e725b4d4ffe2ae9da))
* Add ID-Search for more Searches (Only if Numbers inserted) ([bbc1a49](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/bbc1a49008a6a7bbf248738c4e453861f21053fd))

## [2.10.1](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.10.0...v2.10.1) (2023-07-03)


### :repeat: Build System

* Update to Cypress 12.6 ([2deaf59](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/2deaf59d8adcd52ec939e7960f7431f97cee1a7d))


### :bug: Fixes

* Entfernen von Legacy-Environment-Datei ([e094b28](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/e094b28a23b8b3fd1782930d0fb177911988834a))
* Optimize ChangeDetection ([5984eda](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/5984edabc64b99ee7982fcdc35f4932c97757b79))

## [2.10.0](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.9.1...v2.10.0) (2023-06-23)


### :repeat: CI

* disable Cypress temp ([49ce3ee](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/49ce3ee2bbda63aa73eed6aa8c2bd1d34d03d09a))


### :sparkles: Features

* Add Ukrain-Language-Support ([210c08d](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/210c08d63bfe42100814b5c0cbadde0602f15926))


### :barber: Style

* Reorder Languages to 3 Columns ([c420c8f](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/c420c8f589e0c6b493fae987a93d5f9a1a1619a9))

## [2.9.1](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.9.0...v2.9.1) (2023-06-23)


### :repeat: Build System

* Make db quiet ([13d68a1](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/13d68a190c792e909932ae766e3732a1a1d772af))
* Optimize CY-Calls ([95ee759](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/95ee759df48d9303e8fcd12ed66a38075c3d3173))
* Optimize Konfig ([b9c608f](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/b9c608f09898978eff479b21f5b0f676af8a9bbf))


### :bug: Fixes

* Upgrade to Angular 16.1 ([527c096](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/527c096721d98fc00f530e95fe7528d998681956))
* Upgrade to pnpm ([47d297f](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/47d297f67a2aae4cf301e6898f6ca0938ce2b0b8))

## [2.9.0](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.8.0...v2.9.0) (2023-05-22)


### :sparkles: Features

* Change to Data-Type signals (Angular 16 Feature) ([f162200](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/f16220099740a7017429e6a8745f356a03a564b7))

## [2.8.0](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.7.4...v2.8.0) (2023-05-19)


### :sparkles: Features

* Add Korean ([2c77758](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/2c7775857bf52845e436e835dc5a5ac2d8ce495d))
* Add Norsk ([6d2fdc9](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/6d2fdc9aeba81355151cf8a42108ceb100468873))


### :repeat: Chores

* Add new Languages ([c8a31c8](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/c8a31c8f2b9b5a371e2fd07e61344814071fdba6))
* Preflight Deps ([62a6954](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/62a69542db7d489f33fba69135a079db9690a158))


### :bug: Fixes

* Clear Deps ([15e4c25](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/15e4c25031da7589ccc149e3c98044e0132dcf1b))
* Update Deps to Preflight Angular 16 ([f4ec3d1](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/f4ec3d1b2c98ae1d5835979807d2e29f280e4da7))
* Upgrade to Angular 16 ([18d7472](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/18d74726d2e0e681d6dbf805b938286d59920b44))

## [2.7.4](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.7.3...v2.7.4) (2023-03-06)


### :bug: Fixes

* Optimize Performance with autoDestroy ([592f9f2](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/592f9f2dd4065249f3a09282cd85f9f888919954))

## [2.7.3](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.7.2...v2.7.3) (2023-02-27)


### :bug: Fixes

* Update to Angular 15.2 ([e365af2](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/e365af25db0dd9fdd910989b623854bb689879f3))
* Update to Cypress and Run withit ([538397f](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/538397fa85258306e9ff7e86714a601981fa92ba))
* Update to Jest 29 ([d477ff3](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/d477ff3cb0fdfc477f1b566547e2b099600b6df5))

## [2.7.2](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.7.1...v2.7.2) (2023-02-06)


### :white_check_mark: Tests

* Add Cypress as Quality-Check into Pipeline ([c2c1029](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/c2c1029dd6dce603c96470dd570089de895218d3))
* Update Cypress-Process for QA ([c97e293](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/c97e2932d5f05bd75058d3d3c81c63a9329211fc))


### :bug: Fixes

* Fix little UX-Problems with Upgrade to Angular 15 (Material Design) ([0113c0a](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/0113c0a82cb33527840e34712a91cabb4ce23757))
* Update Cypress ([38ad565](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/38ad565f7869e0f177d44ccb662fc6337f365925))

## [2.7.1](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.7.0...v2.7.1) (2023-01-16)


### :bug: Fixes

* Make better release-Runs ([2ebe64e](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/2ebe64eeadc2a52111fcaec4e94710913f7a6038))
* Remove flex-layout as dependencie ([5402b89](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/5402b89bde62f68efb4fc79dba6d6d486484915f))
* Update to Angular 15.1 ([6cddf35](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/6cddf3569ae830bff777c1cce52a2208a415b753))

## [2.7.0](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.6.0...v2.7.0) (2022-12-28)


### :sparkles: Features

* Update to Angular 15 ([8e78fe1](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/8e78fe1d8875a3ba4546a7044442f34951d6695b))


### :bug: Fixes

* Add testings ([06a530c](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/06a530cb827897401aa737dcbe0d86a305417ba1))
* Migrate Components to MDC-Default ([1ff6781](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/1ff6781263c2501065dd72e992cfbae3776644b0))


### :repeat: Chores

* Set Budget of Styles up to 90kb ([07cb063](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/07cb063d1cec246dc95b19c65b28f8077311c44d))
* Update Dependencies [skip ci] ([41df56b](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/41df56bfc48de8b565ae6311b4835c84f4fe406e))

## [2.6.0](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.5.2...v2.6.0) (2022-07-22)


### :bug: Fixes

* rework translations from bg to it ([1225fa6](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/1225fa6a87309f67da5074b9b42a43ed4aae2c52))


### :repeat: Build System

* Optimize version of versioncontrol ([e85d38f](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/e85d38f53e6dc266fe6246980c87966a67e7006f))
* Update dependencies ([138a161](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/138a161aa06a74a314cd9650eae793a65d1cab50))
* Update Package of Commits ([894386b](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/894386bebe9cc04fc4c76837ee850c67bf80f226))


### :white_check_mark: Tests

* **e2e:** add E2E-tests ([e25619e](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/e25619ed103f497f507a97b583eaf78fdb9dbb0b))
* **e2e:** Update Cypress-Tests ([35bb443](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/35bb4431ce1ae1059bd051c94f0039b98c81ba1f))


### :memo: Documentation

* Update Readme for developer ([1e602c4](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/1e602c4a9de068f8c00bb8f054b4b98406f23cfa))


### :sparkles: Features

* Add Autocomplete for Device-Search ([650643a](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/650643ab9ceec43543f0a8af0458299c88492491))
* Add Settings ([3491593](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/34915930846457ad83d9465d2d531c79a21f2317))
* Optimize Default-Language via Settings ([048c4c4](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/048c4c4cf22e0661ffed80f9318644cb08f0c867))
* **settings:** Add Settings for disable Options ([05966bd](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/05966bd4a7d7c615c7febd0df69ee0e8ccce1e8e))
* Update translations ([30b4845](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/30b4845b0525edd714bdbabca54ff90f007e797c))


### :zap: Reverts

* Revert server-configs ([8c952c7](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/8c952c7ef5f345e4935c69c1ac49d61d448d9c10))

## [2.5.2](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.5.1...v2.5.2) (2022-06-15)


### :bug: Fixes

* Build Frontend to Standalone-Components ([fd65ba0](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/fd65ba0ed7b0ebd24aebef032c330a2b4aae46a8))
* Insert typed Form ([0d1f41b](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/0d1f41bdab87ac70c98fc1a06ff2ce4a6a3a3087))
* Upgrade to Angular 14.0.2 ([cf73f3a](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/cf73f3a1b716a19f65ac508bdfc2b530eb1fd97e))


### :repeat: Build System

* Set Angular in Standalone-Mode ([055c5de](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/055c5deb1044946effbddd1e4928f910cd9b7d01))

## [2.5.1](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.5.0...v2.5.1) (2022-06-03)


### :bug: Fixes

* Fix Language Files ([92f9762](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/92f976217ffbd3c53a033b8632ba7816570dbb88))
* Make Language-Switch more readable with two columns ([2fce93b](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/2fce93be2e244880fc653980c6d5d6455d5ef629))

## [2.5.0](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.4.1...v2.5.0) (2022-06-03)


### :bug: Fixes

* Optimize Dialog-Header ([9fb5cda](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/9fb5cdafa8c536f8aa3da27655203cef043ca38e))
* Optimize Width of Basic-Layout ([d6dc791](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/d6dc791a77ab69c514929bb3df3557b458e587f0))
* Rework Print for Protocoll without popup ([707d2b3](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/707d2b3451c499645a88d2c759d54232c1dec49b))
* Search for Output on enter in Form ([d852b72](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/d852b72c91f04a3020a9d66322ee583042890068))


### :sparkles: Features

* Integrate Language - Bulgarian ([8cb1752](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/8cb1752f06f59521f910ae27583212fe14fbdfe4))
* Integrate Language - Chinese (simplified) ([093e3ad](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/093e3ad6a2f30b8b08392db5b28cd4266cbdf2e0))
* Integrate Language - Czech ([6c45036](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/6c45036f6398475fc34f8901a5550c4018e57dbe))
* Integrate Language - Danish ([56d0032](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/56d0032fcfba7c85f586a8c954ac4b7d480a7d9a))
* Integrate Language - Dutch ([edcfc59](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/edcfc596ec5d4fc604a9220c25b8cc9d530a3c5f))
* Integrate Language - English ([071fd61](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/071fd614da0960b1a6fc5bacbc90bd8e5e5d89d4))
* Integrate Language - Estonian ([516ed70](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/516ed70de6666fffbe146328873ec096c85ad92f))
* Integrate Language - Finnish ([b62c5f0](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/b62c5f020a3732636f2544925c8310ebbb0252d9))
* Integrate Language - French ([448a2c4](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/448a2c4faae514de68e9862a5254dcd59f5155b4))
* Integrate Language - Greek ([03db99f](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/03db99f2a41ee8afd88ddd6aab4651cc2cfac787))
* Integrate Language - Hungarian ([2a3309f](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/2a3309f671dd85f052289334018ac6d3dc3ae516))
* Integrate Language - Indonesian ([a944b3d](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/a944b3d000b42a4f8c30c87c4f18161b44c2fdef))
* Integrate Language - Italian ([9eb8a9f](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/9eb8a9fe86402f404c9f19ad8a83850671edd28e))
* Integrate Language - Japanese ([7b54b9e](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/7b54b9ebf9cec48a81155e4cfd20d8bac8440500))
* Integrate Language - Latvian ([bb47a6e](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/bb47a6ec945fee5446def535b17305c08e739d60))
* Integrate Language - Lithuanian ([22abc8e](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/22abc8e0e533336731b0985145335cac44fd74aa))
* Integrate Language - Polish ([fa01d00](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/fa01d00c1e0e8b641879822a995c977b61836a19))
* Integrate Language - Portuguese ([25fab85](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/25fab8591120d91d087393b226e71c3fd8edaf98))
* Integrate Language - Romanian ([b08763c](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/b08763ca73050e2eec139f8459a6fef27b902e86))
* Integrate Language - Russian ([2fca257](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/2fca2571ef63c2176b5e1748b6346a732d5f120a))
* Integrate Language - Slovak ([6a484b4](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/6a484b423b07cdb464c31faa6668359bdb0306b0))
* Integrate Language - Slovenian ([44948f7](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/44948f70d96ed8072cc9872355698745867eb35e))
* Integrate Language - Spanish ([c77879b](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/c77879b9f3e9496e1b99fdf758c5d432332090ce))
* Integrate Language - Swedish ([50ee922](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/50ee922ab08b458f90210047eaaeddddaa8aa0da))
* Integrate Language - Turkish ([e624266](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/e624266ec1151847040dc9748ced708a587ccebc))
* Integration of Language-Switch and Service-Translation ([8547564](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/85475648337fd58384d88d159457318e906809f1))
* Update Angular 14 (Supported) ([e9f99a2](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/e9f99a2ddb23704f7f31ee048432cb717035ba6f))


### :repeat: CI

* Docker Packages for version updated ([d61553c](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/d61553c3d9b31749235202ab06ffde1b6e7cfed1))
* Docker-Image updated ([95547d6](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/95547d6a6a78348a0f944ab5d27c8c0f65b88e6d))
* remove Cluster-Scanner ([af01794](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/af0179479c2def5b8670c84fb3ae5f0c792fb4b5))

### [2.4.1](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.4.0...v2.4.1) (2022-04-14)


### :bug: Fixes

* Trigger for Rebuild fixed ([5eb4fd0](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/5eb4fd0bb833dc01cf14feb28ad257fde485d2c3))
* Typo-Error in CI-Pipeline ([b83d5cb](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/b83d5cbb13e78d5d94ab7c6efd2bb45e5cfab31c))

## [2.4.0](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.3.2...v2.4.0) (2022-04-13)


### :bug: Fixes

* :lock: optimized in :whale: ([1e71617](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/1e71617e17e1fd8c1225de5d4fcd61321c0e960c))


### :repeat: Chores

* Update packages and Install Test-Server ([faa52e1](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/faa52e19a433765d56cfae34594b05b98f4861ac))


### :sparkles: Features

* Create Notification-component for focused information ([1d018c4](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/1d018c46de5239a9fc0555ae8eaf139040dc9d08))
* Make Input auto-focused ([434f646](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/434f64666b926ab5e6ca981ca1b920c1353112b4))
* Make Software more Project Type-Fixed with Interfaces ([7e22489](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/7e22489af162d527308d331a6648049ddaa7cb93))


### :barber: Style

* create new Style for more Focus ([cd1ed1d](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/cd1ed1d565e309171b446793c39e2c7f14687fe0))


### :repeat: Build System

* Optimize Size of SCSS ([8f72280](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/8f72280ef5c8a5e570b5a0604a830160cd18622f))

### [2.3.2](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.3.1...v2.3.2) (2022-03-20)


### :bug: Fixes

* Fix Path of CI-Script for 🐳 ([9f06522](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/9f06522157eb65eee3e7c39cea74dcacb620be94))
* Fix Security-Issue with Login of 🐳 ([8865984](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/8865984d6ac5ddb93688e2ffd6f50fd2246a18a5))
* Install Licence-Check 📝 ([94a2a41](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/94a2a41d427756426936ee9ec60cb1062026f115))
* Login-Optimize ([fed66ae](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/fed66ae55d1823776c25962aee57b3e15e51d6c7))
* Security-optimize ([95e1689](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/95e1689c10e80dd26abe6cf92ce07bdc90a3d4c7))


### :repeat: CI

* fix Pipeline-error ([bbc8d8c](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/bbc8d8c6c8e9f1a3f0288e81207df4dc2e590688))
* fixed Path-Issue (Permission) ([b630728](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/b6307287a5d7fdf4ec472623b070daf4efd6e0ef))

### [2.3.1](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.3.0...v2.3.1) (2022-03-20)


### :bug: Fixes

* Install Git into 🐳 ([c53adf0](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/c53adf068224b7611428546d2fed86c840eb450f))
* Install-Paths fixed ([e0bf8e3](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/e0bf8e399eb6e3f29c0484108f513c12b6709e82))
* Security-Fixes ([6763533](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/676353381f9d2c4f0ebe3931d00411386861efc7))

## [2.3.0](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.2.7...v2.3.0) (2022-03-19)


### :sparkles: Features

* Integrate Security-Checks ([d51c789](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/d51c789de81e40fefab89863ed180c51f50c4068))
* Upgrade to Angular Version 13.3 ([80d2eab](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/80d2eabbace0a4397776ce99962575cf8a97afbd))


### :bug: Fixes

* Insert own Versioncontrol ([b747f55](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/b747f5545d2a32f29eba8d1e6295e136f243d255))

### [2.2.7](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.2.6...v2.2.7) (2022-01-01)


### :bug: Fixes

* Fix Asset-Path ([136e5c2](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/136e5c26d88d872f8dd563c65066c289459308d6))

### [2.2.6](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.2.5...v2.2.6) (2022-01-01)


### :bug: Fixes

* Change Assets-Name for Compiled Assets ([4328882](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/432888286d1cc838263da31f661a799ab82cfa04))

### [2.2.5](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.2.4...v2.2.5) (2022-01-01)


### :bug: Fixes

* Change Color-System to #FF8118 (Official Color, wrong color on the Website) ([58a0f2d](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/58a0f2dee7c99d3a4e96fcb91c5373fc2d435f76)), closes [#FF8118](https://gitlab.com/mwaldheim/ausgabeschein-generator/issues/FF8118)

### [2.2.4](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.2.3...v2.2.4) (2021-12-19)


### :bug: Fixes

* Korrektur von Breite für Dialoge ([da5054d](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/da5054d5eb6a4c1b2fd7a3bda8cd3c0d6ef03a9c))

### [2.2.3](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.2.2...v2.2.3) (2021-12-19)


### :repeat: Chores

* **release:** 2.2.3-next.1 [skip ci] ([e30110a](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/e30110a35e012d9ca125b5216704604854252992))
* Korrektur von Assets-Ablage ([ddd8c6a](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/ddd8c6a065968ea9e72ca6569ffcb99133c35082))
* Vereinfachen von CI ([1d1bde8](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/1d1bde8053a4332b33e11bba0beb07b134e0e4f9))


### :bug: Fixes

* Design von InterfaceAG implementiert und letzte Optimierungen am den Assets ([82c1e3f](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/82c1e3f5af565e9ad69a1a98028bb666a450ab2c))
* Erweitern von Markdown-Output um Emoji-Darstellung ([9d8388f](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/9d8388f6f36d274645bb602e2d08d9bd54e2bc45))
* Korrektur von Publish-Command ([955f9a8](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/955f9a80b2931dcf055a8d2643a857ba658b096c))
* Remove Output-Folder (.angular) since Angular 13 ([0de13e6](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/0de13e6ff2eb1c5742333b8d6c964c82aa46928b))
* Umbau von Release zum automatischen builden von Version ([9f731c3](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/9f731c3e963db7dbd7ed91f2a4aae27e7cd4136a))
* UX-Optimierung, dass die Maske schlanker wird. Optimierung von Ressourcen-Größe und Mobiloptimierung ([75351a4](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/75351a4478743b2ee45b81f04f49af5839364877))
* Zip builded Assets ([9a20fb5](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/9a20fb55c4e9778bf31f171058f749d2166c9fa0))

### [2.2.3-next.1](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.2.2...v2.2.3-next.1) (2021-12-19)


### :bug: Fixes

* Erweitern von Markdown-Output um Emoji-Darstellung ([9d8388f](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/9d8388f6f36d274645bb602e2d08d9bd54e2bc45))
* Korrektur von Publish-Command ([955f9a8](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/955f9a80b2931dcf055a8d2643a857ba658b096c))
* Remove Output-Folder (.angular) since Angular 13 ([0de13e6](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/0de13e6ff2eb1c5742333b8d6c964c82aa46928b))
* Umbau von Release zum automatischen builden von Version ([9f731c3](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/9f731c3e963db7dbd7ed91f2a4aae27e7cd4136a))
* UX-Optimierung, dass die Maske schlanker wird. Optimierung von Ressourcen-Größe und Mobiloptimierung ([75351a4](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/75351a4478743b2ee45b81f04f49af5839364877))
* Zip builded Assets ([9a20fb5](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/9a20fb55c4e9778bf31f171058f749d2166c9fa0))


### :repeat: Chores

* Korrektur von Assets-Ablage ([ddd8c6a](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/ddd8c6a065968ea9e72ca6569ffcb99133c35082))
* Vereinfachen von CI ([1d1bde8](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/1d1bde8053a4332b33e11bba0beb07b134e0e4f9))

### [2.2.2](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.2.1...v2.2.2) (2021-12-18)


### :bug: Fixes

* Remove Karma for Jest-Testing ([9464bf0](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/9464bf05ce85034b2a38b8982b1ff36e30a13d4c))
* Update Angular from Version 12.1 to 13.1 ([12ed1c3](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/12ed1c3d84b7ac920208c4b671eafce160914938))
* update Semantic-Version to save CI-Time ([a5f88a4](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/a5f88a4589198b17ebc0fe41bbc634acb418a281))

## [2.2.1](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.2.0...v2.2.1) (2021-08-04)


### Bug Fixes

* Plus-Trennung zwischen Name und Model entfernt und durch einen einfachen Whitespace ersetzt. ([37d4a2b](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/37d4a2ba0e4055fd2042d05c6654a250fa039e51))

# [2.2.0](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.1.6...v2.2.0) (2021-08-03)


### Bug Fixes

* Löschen von alten Config-Ordner ([6d5a208](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/6d5a208c108f72d57882ec0630484dd374183650))
* Verschieben von Datum vor den Überlassungstext ([04178ff](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/04178ffb7b27e0775643e7e89a0d6f020e67c90d))


### Features

* Einfügen von neuem Modus für das Überlassungsprotokoll ([3436cc5](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/3436cc5a86c25be61594768562ce0bd5bbb20f0e))
* Erweitern von server.json um Texte für Transfer, Output und Return ([5e7ae95](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/5e7ae95ab4ad20f257fe34edb777a3ae25a219e6))
* Modus 3 in Tool aufgenommen für die Überlassung von Geräten ([0c6d375](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/0c6d3752bcd16d12b64e36b7ed958ce027a83cdc))

## [2.1.6](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.1.5...v2.1.6) (2021-07-27)


### Bug Fixes

* Korrektur von Datumsaufbau im Print-Dialog mit einer korrekten Datumsberechnung und Ausgabe gemäß deutscher Norm ([6a8f3dd](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/6a8f3ddfac67bf6bb8b770892ebee9021b2ab744))
* Styling-Korrektur für den Ausdruck ([ac2e54b](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/ac2e54b316a80d4a12475ee9dc4da77fa244b66d))
* Umbau von Loading der server.json ( **WICHTIG:** server.json befindet sich nun im ROOT-Verzeichnis ([28b7ef2](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/28b7ef20992fa9389bf7589446bf0cfbc66a7796))
* Umdrehen der Dialog-Buttons, zur besseren UX ([3e7a864](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/3e7a86496f7ccc92e47b5205eb115725673baccc))

## [2.1.5](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.1.4...v2.1.5) (2021-07-19)


### Bug Fixes

* Header umgebaut auf einen korrekten Authorization-call ([3c76f10](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/3c76f10fb82f8c61cdadf6df840a1b3330c8bc40))

## [2.1.4](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.1.3...v2.1.4) (2021-07-19)


### Bug Fixes

* Optik in Deeppurple umgestellt ([f0fa296](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/f0fa29648bd60edc435a95942a7dcbab38776ce1))

## [2.1.3](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.1.2...v2.1.3) (2021-07-19)


### Bug Fixes

* Andere Stile eingefügt ([42ddc6a](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/42ddc6a7b76d361349326345790cb67e271697eb))
* Background-Draw korrekt eingefügt ([0b611c0](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/0b611c026db5ff3aedc1a4ba27d81bd26036fc84))
* Row-Design angepasst ([da33223](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/da332239f9548d0f03947f0399286ec5552c74f0))
* Suche korrekt eingebaut ([2da87a8](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/2da87a8e1cc117c47ba690db0199d26ea5e68ccc))

## [2.1.2](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.1.1...v2.1.2) (2021-07-19)


### Bug Fixes

* Root-Verhalten korrigiert ([20a7fbf](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/20a7fbf46754a54c08bb5e34ff41f778110897ef))

## [2.1.1](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.1.0...v2.1.1) (2021-07-18)


### Bug Fixes

* Configs in Ignore aufgenommen ([e56e640](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/e56e640bbb8b85ba78d031d627d29eb30ea27e36))
* Gitkeep für Configs eingefügt ([6abc8aa](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/6abc8aa492d505265b467fb181e09d98e2f52e36))
* Server-Konfigurationen entfernt ([b99c54b](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/b99c54b0db10bc589497c8bdcf5b95f10bc18ec8))

# [2.1.0](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v2.0.0...v2.1.0) (2021-07-18)


### Features

* Logos Updaten ([4cc8d6c](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/4cc8d6cdbbf85b62229814c86c97d3b42165dd9f))

# [2.0.0](https://gitlab.com/mwaldheim/ausgabeschein-generator/compare/v1.0.0...v2.0.0) (2021-07-18)


### Features

* Auslagern von Dialogen ([9d93eae](https://gitlab.com/mwaldheim/ausgabeschein-generator/commit/9d93eae6063a6732eb674836f15f5650f78b6e23))


### BREAKING CHANGES

* Umbau des PrintDialogs auf Angular 12.1.2.

# 1.2.2 (2019-06-15)


### Bug Fixes

* PHP 8.0 vorbereitet

# 1.2.1 (2019-05-23)


### Bug Fixes

* Folder Korrektur

# 1.2.0 (2019-05-23)


### Features

* Include Auto-Testing

# 1.1.2 (2019-05-23)


### Bug fixes

* Automatischer Debug-MODE
* Echo von Variablen in index.php angepasst

# 1.1.1 (2019-05-23)


### Bug fixes

* Suche immer auf UPPERCASE
* Leerstelle zwischen Model und Name in Ausgabe hinzugefügt
* Changelog-GET korrigiert
* In Suche Leerzeichen entfernt
* Fehler von Vorbestückung behoben
* Abfangen von Fehlermeldung des CURL-Aufrufs

# 1.1.0 (2019-05-23)

### Features

* arrGET zu strSearch umgebaut
* Autoaufruf eingebaut
* Unterscheidung zwischen Ausgabe und Rückgabe hinzugefügt
* Styles aus index.php ausgelagert
* Changelogs ausgelagert

### Bug fixes

* Code-Comments gesetzt
* Textanpassungen im PHP
* Settings-Datei überarbeitet
* Texte im Script angepasst
* Fehler von Vorbestückung behoben

# 1.0.7 (2019-05-23)

### Features

* CURL-Aufruf aufgebaut und implementiert

### Bug fixes

* TOKEN ausgelagert

# 1.0.6 (2019-05-23)

### Features

* HTML-Ausgabe für Print Dialog hinzugefügt
* Print Dialog gebaut
* Changelogs aufgesetzt

### Bug fixes

* ’Date-of-Purchase’ auf ’DateOfPurchase’ geändert

# 1.0.5 (2019-05-23)

### Features

* Dialog für Ausgabe aufgebaut

### Bug fixes

* Dialog aus falschen Funktionsaufruf gezogen

# 1.0.4 (2019-05-23)

### Features

* Settings in PHP gebaut

### Bug fixes

* PHP-DebugMode hinzugefügt

# 1.0.3 (2019-05-23)

### Features

* Getter und Setter für PHP-Object hinzugefügt, um geplanten DebugMode zu unterstützen
* Code verschönert

# 1.0.2 (2019-05-23)

### Features

* Statische Meldungen durch dynamische ersetzt
* Deutschübersetzung hinzugefügt
* Curl-Aufruf hinzugefügt

# 1.0.1 (2019-05-23)

### Features

* Ajax-Call aufgebaut zur Verarbeitung der Daten
* Statusmeldung-Handler eingebaut
* Statische Meldungen platziert

# 1.0.0 (2019-05-23)

### Features

* Dialog aufgesetzt
* Form aufgebaut
* Framework initialisiert
* Projekt aufgesetzt
