import {ApplicationConfig, importProvidersFrom} from "@angular/core";
import {routes} from "./app.routes";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { provideHttpClient, withFetch } from "@angular/common/http";
import {provideRouter} from "@angular/router";
import {MarkdownModule} from "ngx-markdown";

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    importProvidersFrom(BrowserAnimationsModule),
    provideHttpClient(withFetch()),
    importProvidersFrom(MarkdownModule.forRoot())
    ]
};
