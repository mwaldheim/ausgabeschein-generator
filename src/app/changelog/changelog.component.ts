import {ChangeDetectionStrategy, Component } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

@Component({
    selector: 'ag-changelog',
    templateUrl: './changelog.component.html',
    styleUrls: [],
    imports: [
        SharedModule
    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChangelogComponent {}
