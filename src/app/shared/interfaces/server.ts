export interface Server {
  url: string;
  token: string;
  company: string;
  address: string;
  output: string;
  return: string;
  transfer: string;
  enable_output: boolean;
  enable_input: boolean;
  enable_transfer: boolean;
  enable_multilanguage: boolean;
  default_language: string;
}
