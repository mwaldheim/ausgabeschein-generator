import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'ag-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: false
})
export class NotificationComponent {}
