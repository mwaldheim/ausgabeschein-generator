import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {LANGUAGES} from "./language";
import {SettingsService} from "../../../settings/settings.service";
import {Directionality} from "@angular/cdk/bidi";
import {Subscription} from "rxjs";


@Component({
    selector: 'ag-language',
    templateUrl: './language.component.html',
    styleUrls: ['./language.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: false
})
export class LanguageComponent implements OnDestroy {
  current: string;
  current_name: string;
  default: string = 'de';
  private isRtl: boolean;

  private _dirChangeSubscription = Subscription.EMPTY;

  languageList: {iso: string, name: string}[] = LANGUAGES;

  constructor(private translate: TranslateService, private settings: SettingsService, private dir: Directionality) {
    this.default = this.settings.settings()?.default_language;
    this.isRtl = dir.value === 'rtl';

    this._dirChangeSubscription = dir.change.subscribe(() => {
      this.flipDirection();
    });

    if (localStorage.getItem('i18n')) {
      // @ts-ignore
      this.translate.use(localStorage.getItem('i18n') ? localStorage.getItem('i18n') : this.default);
      // @ts-ignore
      localStorage.setItem('i18n', localStorage.getItem('i18n') ? localStorage.getItem('i18n') : this.default);
      // @ts-ignore
      this.current = localStorage.getItem('i18n');
      // @ts-ignore
      this.current_name = this.languageList.find(x => x.iso === this.current).name;
    } else {
      this.translate.use(this.default);
      localStorage.setItem('i18n', this.default);
      this.current = this.default;
      // @ts-ignore
      this.current_name = this.languageList.find(x => x.iso === this.current).name;
    }
  }

  flipDirection() {
    this.isRtl = !this.isRtl;
    this.dir.change.emit(this.dir.value === 'rtl' ? 'ltr' : 'rtl');
  }

  ngOnDestroy() {
    this._dirChangeSubscription.unsubscribe();
  }

  changeLang(lang: string) {
    this.current = lang;
    // @ts-ignore
    this.current_name = this.languageList.find(x => x.iso === this.current).name;
    this.translate.use(lang);
    localStorage.setItem('i18n', lang);
  }
}
