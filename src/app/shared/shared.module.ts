import {HttpClient, HttpClientModule} from '@angular/common/http';
import {isDevMode, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ServiceWorkerModule } from '@angular/service-worker';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {MarkdownModule, MARKED_OPTIONS} from 'ngx-markdown';
import { NotificationComponent } from './components/notification/notification.component';
import { LanguageComponent } from './components/language/language.component';
import { PrintDirective } from './directive/print.directive';
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatSelectModule} from "@angular/material/select";
import {MatAutocompleteModule} from "@angular/material/autocomplete";

const components = [
  NotificationComponent,
  LanguageComponent,
  PrintDirective,
];

const modules = [
  CommonModule,
  MatMenuModule,
  MatButtonModule,
  MatDividerModule,
  MatChipsModule,
  MatFormFieldModule,
  MatGridListModule,
  MatInputModule,
  MatIconModule,
  MatToolbarModule,
  MatCardModule,
  ReactiveFormsModule,
  MatProgressSpinnerModule,
  MatSnackBarModule,
  MatIconModule,
  MatDialogModule,
  MatBadgeModule,
  MatTooltipModule,
  MatSlideToggleModule,
  MatSelectModule,
  MatAutocompleteModule,
  HttpClientModule,
];

@NgModule({
    declarations: [
      ...components,
    ],
  exports: [
    ...components,
    ...modules,
    TranslateModule,
    MarkdownModule,
  ],
  imports: [
    ...modules,
    MarkdownModule.forRoot({
      loader       : HttpClient, // optional, only if you use [src] attribute
      markedOptions: {
        provide : MARKED_OPTIONS,
        useValue: {
          gfm        : true,
          breaks     : false,
          pedantic   : false,
        },
      },
    }),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: !isDevMode(),
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
    TranslateModule.forRoot({
      loader: {
        provide   : TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps      : [HttpClient]
      }
    }),
  ],
  providers: [
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2500}},
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true,autoFocus:'first-tabbable'}},
    {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {appearance: 'outline'}}
  ],
})
export class SharedModule { }


// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}
