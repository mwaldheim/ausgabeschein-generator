import {Directive, HostListener, input, Input} from '@angular/core';

@Directive({
    selector: 'button[agPrint]',
    standalone: false
})
export class PrintDirective {
  private _printStyle = [];
  printSectionId = input<string|undefined>();
  printTitle = input<string|undefined>();
  useExistingCss = input<boolean>(false);
  printDelay = input<number>(0);

  @Input()
  set printStyle(values: { [key: string]: { [key: string]: string } }) {
    for (let key in values) {
      if (values.hasOwnProperty(key)) {
        // @ts-ignore
        this._printStyle.push((key + JSON.stringify(values[key])).replace(/['"]+/g, ''));
      }
    }
    this.returnStyleValues();
  }

  public returnStyleValues() {
    return `<style> ${this._printStyle.join(' ').replace(/,/g, ';')} </style>`;
  }

  /**
   *
   *
   * @returns html for the given tag
   *
   * @memberof NgxPrintDirective
   */
  private _styleSheetFile = '';

  /**
   * @memberof NgxPrintDirective
   * @param cssList
   */
  @Input()
  set styleSheetFile(cssList: string) {
    let linkTagFn = function (cssFileName: string) {
      return `<link rel="stylesheet" type="text/css" href="${cssFileName}">`;
    }
    if (cssList.indexOf(',') !== -1) {
      const valueArr = cssList.split(',');
      for (let val of valueArr) {
        this._styleSheetFile = this._styleSheetFile + linkTagFn(val);
      }
    } else {
      this._styleSheetFile = linkTagFn(cssList);
    }
  }

  /**
   * @returns string which contains the link tags containing the css which will
   * be injected later within <head></head> tag.
   *
   */
  private returnStyleSheetLinkTags() {
    return this._styleSheetFile;
  }

  private getElementTag(tag: keyof HTMLElementTagNameMap): string {
    const html: string[] = [];
    const elements = document.getElementsByTagName(tag);
    for (let index = 0; index < elements.length; index++) {
      html.push(elements[index].outerHTML);
    }
    return html.join('\r\n');
  }

  /**
   *
   * @param data the html element collection to save defaults to
   *
   */
  private getFormData(data: any) {
    for (var i = 0; i < data.length; i++) {
      data[i].defaultValue = data[i].value;
      if (data[i].checked) {
        data[i].defaultChecked = true;
      }
    }
  }

  /**
   * @returns html section to be printed along with some associated inputs
   *
   */
  private getHtmlContents() {
    let printContents = undefined;
    if (typeof this.printSectionId() === 'string') {
      printContents = document.getElementById(this.printSectionId()!);
    }
    let innards = printContents?.getElementsByTagName('input');
    this.getFormData(innards);

    let txt = printContents?.getElementsByTagName('textarea');
    this.getFormData(txt);

    return printContents?.innerHTML;
  }

  /**
   *
   * @returns boolean value
   */
  @HostListener('click')
  public print(): boolean {
    let printContents, styles = '', links = '';
    const baseTag = this.getElementTag('base');

    if (this.useExistingCss()) {
      styles = this.getElementTag('style');
      links = this.getElementTag('link');
    }

    printContents = this.getHtmlContents();

    let frame = document.createElement('iframe');
    frame.name = 'print-frame';
    frame.style.position = 'absolute';
    frame.style.top = '-1000000px';
    document.body.appendChild(frame);
    let frameDoc =  frame.contentWindow ? frame.contentWindow : frame.contentDocument;
    // @ts-ignore
    if ('document' in frameDoc) {
      frameDoc?.document.write(`<html><head><title>${this.printTitle() ? this.printTitle() : ""}</title>${baseTag}
          ${this.returnStyleValues()}
          ${this.returnStyleSheetLinkTags()}
          ${styles}
          ${links}</head>`);
      frameDoc?.document.write(`<body onload="window.print();"${printContents}<script defer>
            function triggerPrint(event) {
              window.removeEventListener('load', triggerPrint, false);
              setTimeout(function() {
                closeWindow(window.print());
              }, ${this.printDelay()});
            }
            function closeWindow(){
                window.close();
            }
            window.addEventListener('load', triggerPrint, false);
          </script></body></html>`);
      frameDoc?.document.close();
    }
    setTimeout(function () {
      document.body.removeChild(frame);
    }, 500);
    return false;
  }

}
