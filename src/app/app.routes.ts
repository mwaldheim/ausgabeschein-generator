import {AppComponent} from "./app.component";
import {Routes} from "@angular/router";

export const routes: Routes = [
  {path: '', component: AppComponent,
  children: [
    {path: 'settings', component: AppComponent},
    {path: 'changelog', component: AppComponent}
  ]},
  {path: '**', redirectTo: '', pathMatch: 'full'},
]
