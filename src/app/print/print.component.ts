import {ChangeDetectionStrategy, Component, ElementRef, Inject, OnInit, viewChild, ViewChild} from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SharedModule } from '../shared/shared.module';

@Component({
    selector: 'ag-print',
    templateUrl: './print.component.html',
    styleUrls: [],
    imports: [
        SharedModule
    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PrintComponent implements OnInit {
  mode: number | undefined;
  dop: any;
  devicename: string | undefined;
  name: string | undefined;
  model: string | undefined;
  tenant: string | undefined;
  serial: string | undefined;
  comments: string | undefined;
  asset_tag: string | undefined;
  server: any;

  printview = viewChild<HTMLDivElement>('printview');


  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit(): void {
    if (this.data) {
      let res = this.data.results?.shift();
      const date = new Date(res?.custom_fields.DateOfPurchase);
      if (res) {
        this.devicename = res.name;
        this.name = res.device_type.manufacturer.name;
        this.model = res.device_type.model;
        this.tenant = res.tenant.name;
        this.serial = res.serial;
        this.asset_tag = res.asset_tag;
        this.comments = res.comments;
      }

      this.mode =this. data.mode;
      this.dop = `${date.getDate().toString().padStart(2, '0')}.` +
        `${(date.getMonth() + 1).toString().padStart(2, '0')}.` +
        `${date.getFullYear()}`;
      this.comments = this.comments?.replace(/\n/g, "<br />");
      this.server = this.data.server;
    }
  }

  print() {
    let mywindow = window.open('', 'Print', 'height=600,width=800');
    if (mywindow) {
      mywindow.document.write('<html lang="de"><head><title>Ausdruck</title>');
      mywindow.document.write('<style>body {font-family: Arial, Verdana, sans-serif;font-size: 12pt;}</style>');
      mywindow.document.write('</head><body>');
      mywindow.document.write(<string>this.printview?.prototype.innerHTML);
      mywindow.document.write('</body></html>');

      mywindow.document.close();
      mywindow.focus()
      mywindow.print();
      mywindow.close();
    }

  }
}
