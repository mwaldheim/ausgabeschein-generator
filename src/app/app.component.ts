import {ChangeDetectionStrategy, Component, effect, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TranslateService} from '@ngx-translate/core';
import {ChangelogComponent} from './changelog/changelog.component';
import pack from '../../package.json';
import {NetboxService} from './netbox.service';
import {SettingsComponent} from './settings/settings.component';
import {SettingsService} from './settings/settings.service';
import {SharedModule} from './shared/shared.module';
import {AutoDestroy} from "./shared/core/auto-destroy";
import {Subject, takeUntil} from "rxjs";
import {DOCUMENT} from "@angular/common";
import {Router} from "@angular/router";

@Component({
    selector: 'ag-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [SharedModule],
    providers: [
        NetboxService,
        SettingsService,
    ]
})
export class AppComponent {
  title = 'ausgabeschein-generator';
  loading = false;
  version = '0.0.0';
  formGroup: FormGroup = new FormGroup({});
  // FormControl = new FormControl<string>('', [
  //   Validators.required,
  // ]);
  year = new Date().getFullYear();

  @AutoDestroy destroy$: Subject<void> = new Subject<void>();

  constructor(
    private translate: TranslateService,
    private snackBar: MatSnackBar,
    public srvNetbox: NetboxService,
    public srvSettings: SettingsService,
    public dialog: MatDialog,
    private router: Router,
    @Inject(DOCUMENT) private document: Document,
    private fb: FormBuilder) {
    translate.setDefaultLang('de');
    this.version = pack.version;

    this.formGroup = this.fb.group({
      search: ['', Validators.required],
      explicit: [false]
    });

    effect(() => {
      const noSettings = this.srvSettings.noSettings();
      if (noSettings) {
        this.openSettings();
      }
    });
    // get search from referrer url
    const referrer = this.document.referrer;
    if (referrer) {
      // get last part of url, when it is an id, run search
      const id = referrer.split('/').pop();
      if (id && id.match(/^\d+$/)) {
        this.formGroup.controls.search.setValue(id);
        this.formGroup.controls.explicit.setValue(true);
      }
    }


    this.formGroup.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(({search, explicit}) => {
      this.srvNetbox.search(search, explicit);
    });
  }

  openChangelog() {
    const dialogRef = this.dialog.open(ChangelogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  openSettings() {
    const dialogRef = this.dialog.open(SettingsComponent, {
      maxWidth: '90vw',
      width: '70rem'
    });

    dialogRef.afterClosed().subscribe();
  }

  async makeOutput(number: number, $event: any) {
    $event.preventDefault()
    // Loader
    this.loading = true;
    // Check valid Form
    if (this.formGroup.valid) {
      // Call
      this.srvNetbox.makeRequest(this.formGroup.controls.search.value, this.formGroup.controls.explicit.value, number);
    } else {
      this.translate.get('error.invalid-input').pipe(takeUntil(this.destroy$)).subscribe(text => {
        this.snackBar.open(text);
      });
    }
    // Toast Fehler
    this.loading = false;

  }
}
