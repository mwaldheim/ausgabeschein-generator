import {ChangeDetectionStrategy, Component, effect} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {SharedModule} from '../shared/shared.module';
import {SettingsService} from './settings.service';
import {LANGUAGES} from "../shared/components/language/language";
import {AutoDestroy} from "../shared/core/auto-destroy";
import {Subject, takeUntil} from "rxjs";

@Component({
    selector: 'ag-settings',
    imports: [SharedModule],
    templateUrl: './settings.component.html',
    providers: [
        SettingsService
    ],
    styleUrls: ['./settings.component.scss'],
    changeDetection: ChangeDetectionStrategy.Default
})
export class SettingsComponent {

  languages = LANGUAGES;

  form: FormGroup = new FormGroup({
    url: new FormControl<string>(''),
    token: new FormControl<string>(''),
    company: new FormControl<string>(''),
    address: new FormControl<string>(''),
    output: new FormControl<string>(''),
    return: new FormControl<string>(''),
    transfer: new FormControl<string>(''),
    enable_output: new FormControl<boolean>(false),
    enable_input: new FormControl<boolean>(false),
    enable_transfer: new FormControl<boolean>(false),
    enable_multilanguage: new FormControl<boolean>(false),
    default_language: new FormControl<string>('de')
  });
  downloadJsonHref: SafeUrl | undefined;


  @AutoDestroy destroy$: Subject<void> = new Subject<void>();

  constructor(public settings: SettingsService, private fb: FormBuilder,private sanitizer: DomSanitizer) {
    effect(() => {
      const res = this.settings.settings();
      if (res) {
        this.form = this.fb.group({
          url: [res.url],
          token: [res.token],
          company: [res.company],
          address: [res.address],
          output: [res.output],
          return: [res.return],
          transfer: [res.transfer],
          enable_output: [res.enable_output],
          enable_input: [res.enable_input],
          enable_transfer: [res.enable_transfer],
          enable_multilanguage: [res.enable_multilanguage],
          default_language: [res.default_language ? res.default_language : 'de']
        });
      }
      this.export();

      this.form.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
        this.export();
      });
    });
  }

  export() {
    const theJSON = JSON.stringify(this.form.value);
    this.downloadJsonHref = this.sanitizer.bypassSecurityTrustUrl("data:text/json;charset=UTF-8," + encodeURIComponent(theJSON));
  }
}
