import { TestBed } from '@angular/core/testing';

import { SettingsService } from './settings.service';
import { provideHttpClientTesting } from '@angular/common/http/testing';

describe('SettingsService', () => {
  let service: SettingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({providers: [SettingsService, provideHttpClientTesting]});
    service = TestBed.inject(SettingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
