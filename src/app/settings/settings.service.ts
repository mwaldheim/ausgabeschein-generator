import { HttpClient } from '@angular/common/http';
import {Injectable, OnDestroy, signal, WritableSignal} from '@angular/core';
import {catchError, map, of, Subscription} from 'rxjs';
import { Server } from '../shared/interfaces/server';
import {coerceBooleanProperty} from "@angular/cdk/coercion";

@Injectable()
export class SettingsService implements OnDestroy {
  server: Server | undefined;
  token: string = '';
  url: string | undefined;
  noSettings: WritableSignal<boolean> = signal(false);
  settings: WritableSignal<Server | any | null> = signal(null);

  sSubscription: Subscription | undefined;

  constructor(private http: HttpClient) {
    if (!this.server) {
      this.getServerData();
    }
  }

  ngOnDestroy() {
    if (this.sSubscription) {
      this.sSubscription.unsubscribe();
    }
  }

  getServerData() {
    this.sSubscription = this.http.get<Server | undefined>('server.json')
      .pipe(
        map(response => {
          return response;
        }),
        catchError(() => {
          this.noSettings.set(true);
          this.settings.set(null);
          return of(undefined);
        })
      )
      .subscribe(server => {
        if (server) {
          this.server = server;
          this.token = server.token;
          this.url = server.url;
          this.noSettings.set(false);
          this.settings.set(server);
        } else {
          this.noSettings.set(true);
          this.settings.set(null);
        }
      });
  }

  isAvailable(setting: string): boolean {
    let value = false;
    if (this.settings()) {
      value = coerceBooleanProperty(this.settings()[setting]);
    }
    return value;
  }
}
