import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Injectable, isDevMode} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TranslateService} from '@ngx-translate/core';
import {PrintComponent} from './print/print.component';
import {SettingsService} from './settings/settings.service';
import {Netbox} from './shared/interfaces/netbox';
import {BehaviorSubject, Observable} from "rxjs";

@Injectable()
export class NetboxService {
  data: Netbox | {} = {};
  results = new BehaviorSubject<string[]>([]);

  constructor(private http: HttpClient,
              private snackBar: MatSnackBar,
              public dialog: MatDialog,
              private translate: TranslateService,
              private settings: SettingsService) {
  }

  search(search: string | null, explicit: boolean) {
    if (search && search.length > 1) {
      try {
        this.buildRequest(search, explicit).subscribe(data => {
          this.data = data;
          if (data.results) {
            let results = [];
            for (let res of data.results) {
              results.push(res.name);
            }
            this.results.next(results);
          }
        });
      } catch (e) {
        this.translate.get('error.no-connection').subscribe(text => {
          this.snackBar.open(text);
        });
      }
    }
  }

  makeRequest(search: string | null, explicit: boolean, mode: number) {
    try {
      this.buildRequest(search, explicit, '__ie').subscribe(data => {
        this.data = data;

        if (data.count > 0) {
          const dialogRef = this.dialog.open(PrintComponent, {

            maxWidth: '90vw',
            width: '80rem',
            data: Object.assign(data, {mode: mode}, {server: this.settings.server})
          });

          dialogRef.afterClosed();
        } else {
          this.translate.get('error.no-results').subscribe(text => {
            this.snackBar.open(text);
          });
        }
      });
    } catch (e) {
      this.translate.get('error.no-connection').subscribe(text => {
        this.snackBar.open(text);
      });
    }
  }

  buildRequest(search: string | null, explicit: boolean, filter = '__ic'): Observable<Netbox> {
    if (!isDevMode()) {
      const headerDict = {
        'Content-Type': 'application/json',
        'Authorization': this.settings.token
      }
      const requestOptions = {
        headers: new HttpHeaders(headerDict),
      };
      return !explicit
        ? this.http.get<Netbox>(`${this.settings.url}?name${filter}=${search?.toUpperCase().replace('/\s/', '')}`, requestOptions)
        : this.http.get<Netbox>(`${this.settings.url}/${search?.toUpperCase().replace('/\s/', '')}`, requestOptions);

    } else {
      // return this.http.get<Netbox>('http://localhost:3004/sonder');
      return this.http.get<Netbox>('http://localhost:3004/normal');
    }
  }
}
