import { HttpClientTestingModule, provideHttpClientTesting } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { TranslateService } from '@ngx-translate/core';

import { NetboxService } from './netbox.service';
import { SharedModule } from './shared/shared.module';
import { SettingsService } from './settings/settings.service';

describe('NetboxService', () => {
  let service: NetboxService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule],
      providers: [TranslateService, NetboxService, provideHttpClientTesting, SettingsService]
    });
    service = TestBed.inject(NetboxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
